import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  citiesCollection:AngularFirestoreCollection;
  userId;
  private URL = "https://ri3qwl6g5i.execute-api.us-east-1.amazonaws.com/examA-evgeni";

  constructor(private db: AngularFirestore, private http:HttpClient, public Auth:AuthService) { }

  addCity(userId:string){
    var city  = {city:'London'} 
    this.db.collection(`users/${userId}/cities/`).add(city);

    console.log(city);
    var city  = {city:'Paris'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Tel Aviv'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Jerusalem'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Berlin'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Rome'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Dubai'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
    var city  = {city:'Athens'} 
    this.db.collection(`users/${userId}/cities/`).add(city);
  }

  public getCities(userId){
    this.citiesCollection = this.db.collection(`users/${userId}/cities`, 
    ref => ref.orderBy('city', 'asc'));     
    return this.citiesCollection.snapshotChanges();
   }

   updateResult(userId:string,  id:string, temperature ,humidity, result,image){
    this.db.doc(`users/${userId}/cities/${id}`).update(
      {
        result:result,
        temperature:temperature,
        humidity:humidity,
        image:image,
        saved:true,


     

      }
    )
  }

  reset(userId:string,  id:string){
    this.db.doc(`users/${userId}/cities/${id}`).update(
      {
        result:null,
        temperature:null,
        humidity:null,
        saved:null,
        image:null


     

      }
    )
  }

   predict(temperature:number, humidity:number){
  
    let json = {
     "temperature": temperature,
     "humidity": humidity
   }

    let body = JSON.stringify(json);
    return this.http.post<any>(this.URL, body).pipe(map(res =>{
      console.log(res);
      let final:string = res.result;
      console.log(final);
      /*final  = final.replace('[',"");
      final  = final.replace(']',"");
      console.log(final);*/

      return final;

    })
    )
  }







}
