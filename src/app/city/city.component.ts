import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CitiesService } from '../cities.service';
import { City } from '../interfaces/city';
import { Weather } from '../interfaces/weather';
import { WeatherService } from '../weather.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  displayedColumns: string[] = ['position', 'city', 'getdata','temp','humidity', 'image','predict', 'result'];
  userId;
  cities;
  cities$;
  data:any;
  dataState:Array <boolean> = [];
  pred:Array <boolean> = [];

  city:string;
  temperature:number;
  image;
  weatherData$:Observable<Weather>;
  hasError:boolean = false;
  errorMassage:string;

  country:string;
  longitude:number;
  latitude:number;
  humidity:number;

  getData(index:string){
    this.weatherData$ = this.weatherService.searchWeatherData(this.cities[index].city)
    this.weatherData$.subscribe(
    (data) => {this.temperature = Math.round(data.temperature);
        this.data=data; /* anothoer way to capture all the data  or:*/
    this.image = data.image ;
    this.country = data.country;
    this.cities[index].temperature  = this.temperature;
    this.cities[index].humidity  = data.humidity;
    this.cities[index].image  = this.image;
this.dataState[index] = true
this.pred[index] = true


  },
  (error) => {
      console.log(error.message);
      this.hasError = true;
      this.errorMassage = error.message;
    }
  )  
}

predict(temperature:number, humidity:number, index:number){
  this.CitiesService.predict(temperature,humidity).subscribe(
    (res) => {
      console.log(res)
      if(Number(res)<0.5){
      this.cities[index].result = 'will not drop'; }if(Number(res)>0.5){
        this.cities[index].result = 'will drop'
      }

      
      this.dataState[index] = false

    }
  )

}


updateResult(id:string, temperature, humidity, result, index, image){
  this.CitiesService.updateResult(this.userId, id,temperature ,humidity, result,image);
  this.cities[index].saved = true; 
  this.pred[index] = false
  this.dataState[index] = false

  

}

newPredict(index){
  this.CitiesService.reset(this.userId, this.cities[index].id);

  this.pred[index] = false
  
  
  
 
}
  constructor(private CitiesService:CitiesService, public Auth:AuthService,private weatherService:WeatherService) { }

  ngOnInit(): void {
  
    this.Auth.getUser().subscribe(
      (user) =>{
        this.userId = user.uid;
        console.log(this.userId);
          this.cities$ = this.CitiesService.getCities(this.userId);
          this.cities$.subscribe(
            docs => {         
              this.cities = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const city:City = document.payload.doc.data();
                city.id = document.payload.doc.id;
                   this.cities.push(city); 
           }
         }
       ) 
     }
   )
 
 }

}
