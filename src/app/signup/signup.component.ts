import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CitiesService } from '../cities.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string;
  hasError:boolean = false;
  errorMassage:string;
  userId;

  onSubmit(){
    if(this.password.length < 8){
      this.hasError=true;
      this.errorMassage = 'The password must contain at least 8 characters';
    }else{
    this.Auth.signup(this.email, this.password).then(res => {
      this.Auth.getUser().subscribe(
        (user) =>{
          this.userId = user.uid;
          console.log(user.uid)

          this.CitiesService.addCity(this.userId);
        })
       
      this.router.navigate(['/welcome']); 
    }).catch(() => {
      this.hasError=true;
      this.errorMassage = 'This email is already in use by another account';
    });
  }
  }

  constructor(private CitiesService:CitiesService, private Auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
